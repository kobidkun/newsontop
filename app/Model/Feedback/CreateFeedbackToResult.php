<?php

namespace App\Model\Feedback;

use Illuminate\Database\Eloquent\Model;

class CreateFeedbackToResult extends Model
{
    protected $fillable = [
        'name',
        'email',
        'mobile',
        'description',
        'others',
        'path',
        'location',
        'feedback_id',
    ];
}
