<?php

namespace App\Model\Feedback;

use Illuminate\Database\Eloquent\Model;

class CreateFeedback extends Model
{
    public function create_feedback_to_results()
    {
        return $this->hasMany('App\Model\Feedback\CreateFeedbackToResult', 'id', 'feedback_id');
    }

}
