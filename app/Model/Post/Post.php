<?php

namespace App\Model\Post;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function primary_category_to_posts()
    {
        return $this->hasOne('App\Model\Post\PrimaryCategoryToPost','post_id', 'id');
    }

    public function tag_to_posts()
    {
        return $this->hasMany('App\Model\Post\TagToPost','post_id', 'id');
    }

    public function post_to_primary_imagebgs()
    {
        return $this->hasOne('App\Model\Post\PostToPrimaryImagebg','post_id', 'id');
    }

    public function post_to_primary_image_smalls()
    {
        return $this->hasOne('App\Model\Post\PostToPrimaryImageSmall','post_id', 'id');
    }
}
