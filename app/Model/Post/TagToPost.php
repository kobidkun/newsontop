<?php

namespace App\Model\Post;

use Illuminate\Database\Eloquent\Model;

class TagToPost extends Model
{
    protected $fillable = [
        'name',
        'slug',
        'id'
    ];
}
