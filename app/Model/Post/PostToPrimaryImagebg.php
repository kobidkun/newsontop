<?php

namespace App\Model\Post;

use Illuminate\Database\Eloquent\Model;

class PostToPrimaryImagebg extends Model
{
    public function post()
    {
        $this->belongsTo('Models\Post','id','post_id');
    }
}
