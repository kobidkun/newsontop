<?php

namespace App\Http\Controllers\Ad;

use App\Model\Ads\Ad as Advert;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Manageads extends Controller
{


    public function viewPage()
    {

        return view('admin.page.ads.all');


    }


    public function Uploadsadsimage($id, Request $request)
    {

        //   echo 'asasasas';

        $path2 = $request->file('qqfile')->store('ads/images', 'public');


        $c = Advert::where('ad_code', $id)->first();
        // $c = new Advert();

        $c->path = $path2;
        $c->location = $path2;
        $c->ad_code = $id;
        $c->save();


        return response()->json([
            "success" => true,
            'path' => $path2

            , 200]);


    }


    public function adsbyid($id)
    {

        $c = Advert::where('ad_code', $id)->first();


        return response()->json($c, 200);


    }


}
