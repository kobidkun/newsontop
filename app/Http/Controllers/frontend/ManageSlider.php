<?php

namespace App\Http\Controllers\frontend;

use App\Model\Homepagebanner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class ManageSlider extends Controller
{
    public function Index()
    {

        return view('admin.page.Slider.all');

    }


    public function Create()
    {

        return view('admin.page.Slider.create');


    }

    public function Store(Request $request)
    {


        $path2 = $request->file('bgimage')->store('slider/homepage', 'public');


        $a = new Homepagebanner();

        $a->name = $request->name;

        $a->landingurl = $request->landingurl;
        $a->path = $path2;


        $a->location = $path2;

        $a->path = 'slider/homepage/';

        $a->save();

        return back();


    }

    public function delete($id)
    {


        $d = Homepagebanner::findorfail($id);
        $d->delete();
        return redirect(route('slider.index'));

    }

    public function datatables()
    {

        $invoices = Homepagebanner::select(
            [
                'id',
                'name',
                'created_at',
                'location',
                'landingurl',
            ]);

        return DataTables::of($invoices)
            ->addColumn('delete', function ($invoice) {
                return '<a href="' . route('slider.delete', $invoice->id) . '" class=" btn btn-xs btn-danger" title="Delete"><i class="la la-edit"></i>Delete</a>';
            })
            ->addColumn('image', function ($invoice) {

                // location

                return '<img src="' . '/storage/' . $invoice->location . '" width="250px">';


            })
            ->rawColumns(['delete', 'image'])
            ->orderColumn('id', 'created_at $1')
            ->make();
    }

}
