<?php

namespace App\Http\Controllers\Category;

use App\Model\Category\PrimaryCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class ManageCategory extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.page.category.all');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.page.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $a = new PrimaryCategory();
        $a->description = $request->description;
        $a->slug = $request->slug;
        $a->name = $request->name;
        $a->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $a = PrimaryCategory::findorfail($id);
        return view('admin.page.category.edit')->with(['cat' => $a]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $a = PrimaryCategory::findorfail($id);
        return view('admin.page.category.edit')->with(['cat' => $a]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $a = PrimaryCategory::findorfail($id);
        $a->description = $request->description;
        $a->slug = $request->slug;
        $a->name = $request->name;
        $a->save();
        return  redirect( route('category.create'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function datatables()
    {

        $invoices = PrimaryCategory::select(
            [
                'id',
                'name',
                'created_at',
            ]);

        return Datatables::of($invoices)



            /*'.route('category.edit',$invoice->id).'*/



         ->addColumn('action', function ($invoice) {
                return '<a href="'.route('category.edit',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })


          ->rawColumns(['action'])
         ->orderColumn('id', 'created_at $1')
            ->make();
    }

}
