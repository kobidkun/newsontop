<?php

namespace App\Http\Controllers\Feedback;

use App\Model\Feedback\CreateFeedback;
use App\Model\Feedback\CreateFeedbackToResult;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;

class ManageFeedback extends Controller
{
    public function Index()
    {

        return view('admin.page.Feedback.all');

    }


    public function Create()
    {


        return view('admin.page.Feedback.create');


    }

    public function Store(Request $request)
    {


        $path2 = $request->file('bgimage')->store('feedback/upload', 'public');


        $a = new CreateFeedback();

        $a->name = $request->name;
        $a->title = $request->title;
        $a->description = $request->description;
        $a->text = $request->text;
        $a->others = $request->others;
        $a->feedback_id = $request->feedback_id;


        // $a->landingurl = $request->landingurl;


        $a->location = $path2;

        $a->path = 'feedback/upload/';

        $a->save();

        return back();


    }

    public function delete($id)
    {


        $d = CreateFeedback::find($id);
        $d->delete();
        return redirect(route('feedback.index'));

    }


    public function details($id)
    {


        $d = CreateFeedback::find($id);


        $e = CreateFeedbackToResult::where('feedback_id', $id)->get();

        return view('admin.page.Feedback.details')->with(['feedback' => $d, 'details' => $e]);

    }

    public function datatables()
    {

        $invoices = CreateFeedback::select(
            [
                'id',
                'name',
                'created_at',
                'location',

            ]);

        return DataTables::of($invoices)
            ->addColumn('delete', function ($invoice) {
                return '<a href="' . route('feedback.delete', $invoice->id) . '" class=" btn btn-xs btn-danger" title="Delete"><i class="la la-edit"></i>Delete</a>';
            })
            ->addColumn('details', function ($invoice) {
                return '<a href="' . route('feedback.details', $invoice->id) . '" class=" btn btn-xs btn-success" title="Details"><i class="la la-edit"></i>Details</a>';
            })
            ->addColumn('image', function ($invoice) {

                // location

                return '<img src="' . '/storage/' . $invoice->location . '" width="250px">';


            })
            ->rawColumns(['delete', 'image', 'details'])
            ->orderColumn('id', 'created_at $1')
            ->make();
    }
}
