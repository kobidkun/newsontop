<?php

namespace App\Http\Controllers\PublicController;

use App\Model\Category\PrimaryCategory;
use App\Model\Post\Post;
use App\Model\Post\PostToPrimaryImagebg;
use App\Model\Post\PostToPrimaryImageSmall;
use App\Model\Post\PrimaryCategoryToPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;


class ManageNews extends Controller
{
    public function GetAllNews(){
        $a = Post::where('activate', '1')->select(
            [
                'id','title','slug','created_at'
            ]
        )
            ->with('primary_category_to_posts','post_to_primary_imagebgs','post_to_primary_image_smalls','tag_to_posts')->latest()
            ->paginate(8);

        return response()->json($a);
    }


    public function Categorywise($slug){
        $a = PrimaryCategory::where('slug', $slug)->first();
        $d = $a->id;
        $b = Post::where('primary_category_to_post_id','=', $d)->where('activate','=', '1')
            ->with('primary_category_to_posts','post_to_primary_imagebgs','post_to_primary_image_smalls','tag_to_posts')->latest()
            ->paginate(12);

        return response()->json($b);
    }

    public function Categorywisebyid($id){
        $a = PrimaryCategory::where('id', $id)->first();
        $d = $a->id;
        $b = Post::where('primary_category_to_post_id','=', $d)->where('activate','=', '1')
            ->with('primary_category_to_posts','post_to_primary_imagebgs','post_to_primary_image_smalls','tag_to_posts')->latest()
            ->paginate(12);

        return response()->json($b);
    }

    public function Details($slug){

        $a = Post::where('slug', $slug)->where('activate','=', '1')
            ->with('primary_category_to_posts','post_to_primary_imagebgs','post_to_primary_image_smalls','tag_to_posts')
            ->first();

        return response()->json($a);


    }

    public function PrimaryImageUpload(Request $request,$id){

        $path2 = $request->file('qqfile')->store('news/bgimage/images','public');






     $ab =  PostToPrimaryImagebg::where('post_id', $id)->first();



       $ab->location = $path2;



      $ab->save();




        return response()->json([
            'success' => 'true',
            $path2
            ,200]);

    }

    public function SecondaryImageUpload(Request $request,$id){

        $date = time();


        $img =    Image::make($request->file('qqfile'))->encode('jpg', 75)->resize(1000, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save('newsimages/'.$date.'.jpg','public');






        $ac =  PostToPrimaryImageSmall::where('post_id', $id)->first();



        $ac->location = 'newsimages/'.$date.'.jpg';



        $ac->save();


        return response()->json([
            'success' => 'true',
            ''
            ,200]);

    }
}
