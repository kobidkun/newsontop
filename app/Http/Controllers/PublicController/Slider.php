<?php

namespace App\Http\Controllers\publiccontroller;

use App\Model\Homepagebanner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Slider extends Controller
{
    public function HomepageSlider()
    {
        $a = Homepagebanner::all();

        return response()->json($a, 200);
    }
}
