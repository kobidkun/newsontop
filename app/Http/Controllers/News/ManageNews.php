<?php

namespace App\Http\Controllers\News;

use App\Model\Category\PrimaryCategory;
use App\Model\Post\Post;
use App\Model\Post\PostToPrimaryImagebg;
use App\Model\Post\PostToPrimaryImageSmall;
use App\Model\Post\PrimaryCategoryToPost;
use App\Model\Post\TagToPost;
use App\Model\Tags\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use Jelovac\Bitly4laravel\Bitly4laravel;
use Toolkito\Larasap\SendTo;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use Input;
use Bitly;
class ManageNews extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {




        return view('admin.page.news.all');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $c = PrimaryCategory::all();
        $t = Tag::all();
       return view('admin.page.news.create')->with([
           'cs' => $c,
           'ts' => $t,
       ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $a = new Post();
        $a->title = $request->title;
        $a->slug = $request->slug;
        $a->description = $request->description;
        $a->primary_category_to_post_id = $request->primary_category_to_post_id;
        $a->save();




        $lastsaved = $a->id;

        $tagsreq =  $request->input('tags');


        $findcat = PrimaryCategory::findorfail($request->primary_category_to_post_id)->first();


        $postcat = new PrimaryCategoryToPost();
        $postcat->name = $findcat->name;
        $postcat->slug = $findcat->slug;
        $postcat->description = $findcat->description;
        $postcat->post_id = $lastsaved;
        $postcat->save();




        $tags =  explode(",", $tagsreq);

      foreach($tags as $tag){
            $tag2 = new TagToPost();
            $tag2->post_id = $a->id;
            $tag2->name = $tag;
            $tag2->slug = $tag;
            $tag2->description = $tag;
            $tag2->save();
        }








      //  return response()->json($tags);













        $path2 = $request->file('bgimage')->store('news/bgimage/images','public');


      $date = time();


        $img =    Image::make($request->file('smallimage'))->resize(300, null, function ($constraint) {
        $constraint->aspectRatio();
    })->save('newsimages/'.$date.'.jpg','public');








       $ab = new PostToPrimaryImagebg();

        $ab->name = $request->title;
        $ab->post_id = $lastsaved;

        $ab->location = $path2;

        $ab->path = 'news/bgimage/images/';

        $ab->save();


        $ac = new PostToPrimaryImageSmall();

        $ac->name = $request->title;
        $ac->post_id = $lastsaved;

        $ac->location = 'newsimages/'.$date.'.jpg';

        $ac->path = 'newsimages/';

        $ac->save();




        $return_id = $a->id;


        return redirect(route('news.edit',$return_id));









        }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $a = Post::findorfail($id);

        return view('admin.page.news.view')->with(['n' => $a]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $a = Post::findorfail($id);

        return view('admin.page.news.edit')->with(['n' => $a]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $a =  Post::findorfail($id);
        $a->title = $request->title;

        $a->description = $request->description;
        $a->save();















        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $d = Post::findorfail($id);
        $d->delete();
        return redirect(route('news.index'));
    }


        public function datatables()
    {

        $invoices = Post::select(
            [
                'id',
                'title',
                'slug',
                'created_at',
            ]);

        return DataTables::of($invoices)



            /*'.route('category.edit',$invoice->id).'*/



            ->addColumn('action', function ($invoice) {
                return '<a href="'.route('news.edit',$invoice->id).'" class=" btn btn-xs btn-primary" title="View details"><i class="la la-edit"></i>View Details</a>';
            })

            ->addColumn('delete', function ($invoice) {
                return '<a href="'.route('delete.news',$invoice->id).'" class=" btn btn-xs btn-danger" title="Delete"><i class="la la-edit"></i>Delete</a>';
            })

            ->addColumn('tags', function ($invoice) {

                $a = TagToPost::where('post_id', $invoice->id)
                    ->select([
                        'name',
                    ])
                    ->get();

                // return $a;


                return $invoice->tag_to_posts->map(function($tag_to_posts) {
                    return ('<span class="badge badge-primary">'.$tag_to_posts->name.'</span>');
                })->implode('<br>');






                //  foreach ($a as $p) {


                // return '<button type="button" class="btn m-btn--pill    btn-outline-success">' . $p->name .' '.$p->color.' '.$p->size .'</button>';

                //   }
                //    return $k;




            })


            ->rawColumns(['action','delete','tags'])
            ->orderColumn('id', 'created_at $1')
            ->make();
    }

    public function PrimaryImageUpload(Request $request){

        $path2 = $request->file('qqfile')->store('news/bgimage/images','public');

        return response()->json($path2);

    }

    public function activate($id){
        $a = Post::findorfail($id);

        $a->activate = '1';

        $a->save();

        return back();
    }

    public function deactivate($id){
        $a = Post::findorfail($id);

        $a->activate = '0';

        $a->save();

        return back();
    }

    public function PostFacebook($id)
    {

        $post = Post::findorfail($id);

        $pathimage = 'https://manage.newsontop.news/storage/';

        $image = $post->post_to_primary_imagebgs->location;


        $slug = $post->slug;

        $pathnewsdetails = 'https://newsontop.news/details/' . $slug;

        $bitlyurl = Bitly::shorten($pathnewsdetails);
        // $bitlyurl = Bitly::shorten('http://google.com/');


        //  dd($bitlyurl->data->url);

        $smallurl = $bitlyurl->data->url;

        SendTo::Facebook(
            'photo',
            [
                'photo' => public_path('storage/' . $image),
                'message' => $post->title . ' ' . $smallurl
            ]
        );


        /* SendTo::Facebook(
             'link',
             [
                 'link' => $smallurl,
                 'message' => $post->title . ' ' . $smallurl
             ]
         );*/

        // $parsejson = json_decode($bitlyurl->data, true);


        return redirect(route('news.edit', $post->id));
    }


    public function Posttwitter($id)
    {

        $post = Post::findorfail($id);

        $pathimage = 'https://manage.newsontop.news/storage/';


        $slug = $post->slug;

        $pathnewsdetails = 'https://newsontop.news/details/' . $slug;

        $bitlyurl = Bitly::shorten($pathnewsdetails);
        // $bitlyurl = Bitly::shorten('http://google.com/');


        //  dd($bitlyurl->data->url);

        $smallurl = $bitlyurl->data->url;

        $image = $post->post_to_primary_imagebgs->location;


        /*  SendTo::Twitter($post->title . ' ' . $smallurl);*/


        SendTo::Twitter(
            $post->title . ' ' . $smallurl,
            [
                public_path('storage/' . $image),
            ]
        );

        // $parsejson = json_decode($bitlyurl->data, true);


        return redirect(route('news.edit', $post->id));
    }

}
