<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateFeedbackToResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_feedback_to_results', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->text('email')->nullable();
            $table->text('mobile')->nullable();
            $table->text('description')->nullable();
            $table->text('others')->nullable();
            $table->text('path')->nullable();
            $table->text('location')->nullable();
            $table->text('feedback_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_feedback_to_results');
    }
}
