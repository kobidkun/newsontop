<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCreateFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('create_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->text('title');
            $table->text('description')->nullable();
            $table->text('text')->nullable();
            $table->text('path')->nullable();
            $table->text('location')->nullable();
            $table->text('others')->nullable();
            $table->text('feedback_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('create_feedbacks');
    }
}
