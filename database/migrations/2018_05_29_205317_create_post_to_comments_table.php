<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostToCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_to_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name')->nullable();
            $table->text('city')->nullable();
            $table->text('email')->nullable();
            $table->text('mobile')->nullable();
            $table->text('comment')->nullable();
            $table->text('post_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_to_comments');
    }
}
