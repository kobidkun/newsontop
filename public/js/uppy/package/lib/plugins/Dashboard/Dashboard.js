var FileList = require('./FileList');
var Tabs = require('./Tabs');
var FileCard = require('./FileCard');
var classNames = require('classnames');

var _require = require('../../core/Utils'),
    isTouchDevice = _require.isTouchDevice;

var _require2 = require('preact'),
    h = _require2.h;

// http://dev.edenspiekermann.com/2016/02/11/introducing-accessible-modal-dialog
// https://github.com/ghosh/micromodal

var renderInnerPanel = function renderInnerPanel(props) {
  return h(
    'div',
    { style: { width: '100%', height: '100%' } },
    h(
      'div',
      { 'class': 'uppy-DashboardContent-bar' },
      h(
        'div',
        { 'class': 'uppy-DashboardContent-title' },
        props.i18n('importFrom'),
        ' ',
        props.activePanel ? props.activePanel.name : null
      ),
      h(
        'button',
        { 'class': 'uppy-DashboardContent-back',
          type: 'button',
          onclick: props.hideAllPanels },
        props.i18n('done')
      )
    ),
    props.getPlugin(props.activePanel.id).render(props.state)
  );
};

var poweredByUppy = function poweredByUppy(props) {
  return h(
    'a',
    { href: 'https://uppy.io', rel: 'noreferrer noopener', target: '_blank', 'class': 'uppy-Dashboard-poweredBy' },
    'Powered by ',
    h(
      'svg',
      { 'aria-hidden': 'true', 'class': 'uppy-Dashboard-poweredByIcon', width: '12', height: '12', viewBox: '0 0 12 12', xmlns: 'http://www.w3.org/2000/svg' },
      h('path', { 'fill-rule': 'nonzero', d: 'M8.57 7.554v4.149H3.424V7.554H0L6 0l6 7.554H8.57z' })
    ),
    h(
      'span',
      { 'class': 'uppy-Dashboard-poweredByUppy' },
      'Uppy'
    )
  );
};

module.exports = function Dashboard(props) {
  var dashboardClassName = classNames({ 'uppy-Root': props.isTargetDOMEl }, 'uppy-Dashboard', { 'Uppy--isTouchDevice': isTouchDevice() }, { 'uppy-Dashboard--modal': !props.inline }, { 'uppy-Dashboard--wide': props.isWide });

  return h(
    'div',
    { 'class': dashboardClassName,
      'aria-hidden': props.inline ? 'false' : props.modal.isHidden,
      'aria-label': !props.inline ? props.i18n('dashboardWindowTitle') : props.i18n('dashboardTitle'),
      onpaste: props.handlePaste },
    h('div', { 'class': 'uppy-Dashboard-overlay', tabindex: '-1', onclick: props.handleClickOutside }),
    h(
      'div',
      { 'class': 'uppy-Dashboard-inner',
        'aria-modal': !props.inline && 'true',
        role: !props.inline && 'dialog',
        style: {
          width: props.inline && props.width ? props.width : '',
          height: props.inline && props.height ? props.height : ''
        } },
      h(
        'button',
        { 'class': 'uppy-Dashboard-close',
          type: 'button',
          'aria-label': props.i18n('closeModal'),
          title: props.i18n('closeModal'),
          onclick: props.closeModal },
        h(
          'span',
          { 'aria-hidden': 'true' },
          '\xD7'
        )
      ),
      h(
        'div',
        { 'class': 'uppy-Dashboard-innerWrap' },
        h(Tabs, props),
        h(FileCard, props),
        h(
          'div',
          { 'class': 'uppy-Dashboard-filesContainer' },
          h(FileList, props)
        ),
        h(
          'div',
          { 'class': 'uppy-DashboardContent-panel',
            role: 'tabpanel',
            id: props.activePanel && 'uppy-DashboardContent-panel--' + props.activePanel.id,
            'aria-hidden': props.activePanel ? 'false' : 'true' },
          props.activePanel && renderInnerPanel(props)
        ),
        h(
          'div',
          { 'class': 'uppy-Dashboard-progressindicators' },
          props.progressindicators.map(function (target) {
            return props.getPlugin(target.id).render(props.state);
          })
        )
      ),
      props.proudlyDisplayPoweredByUppy && poweredByUppy(props)
    )
  );
};
//# sourceMappingURL=Dashboard.js.map