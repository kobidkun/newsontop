'use-strict';
/**
 * Manages communications with Uppy Server
 */

var RequestClient = require('./RequestClient');
var Provider = require('./Provider');

module.exports = {
  RequestClient: RequestClient, Provider: Provider
};
//# sourceMappingURL=index.js.map