@extends('admin.base')

@section('content')

    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Create Tag</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">pages</li>
                    <li class="breadcrumb-item active">Animation</li>
                </ol>
            </div>
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- Row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create New Tag</h4>
                            <h6 class="card-subtitle">Don't Re create Same Tag</h6>
                            <form class="form p-t-20"

                                  role="form" method="post"
                                  action="{{ route('tag.store') }}"
                            >
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputuname">Tag Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-marker-alt"></i></div>
                                        <input type="text" class="form-control" name="name" placeholder="Tag Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Tag Slug</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-vector"></i></div>
                                        <input type="text" class="form-control" name="slug" placeholder="Tag Slug">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pwd1">Tag Description</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-more-alt"></i></div>
                                        <input type="text" class="form-control" name="description" placeholder="Tag Description ">
                                    </div>
                                </div>


                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                <button type="reset" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Tag List</h4>




                            <div class="table-responsive m-t-40">



                                <table id="product-table" class="display nowrap table table-hover table-striped table-bordered"
                                cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Details</th>

                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Date</th>
                                        <th>Details</th>

                                    </tr>
                                    </tfoot>

                                </table>


                              {{--  <table class="table table-striped table-bordered" id="product-table">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th>Invoice ID</th>
                                        <th >Date</th>

                                        <th>DETAILS</th>



                                    </tr>
                                    </thead>
                                </table>--}}






                            </div>














                            </div>
                    </div>
                </div>

            </div>
            <!-- Row -->

            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right sidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->

            <!-- ============================================================== -->
            <!-- End Right sidebar -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->

    @endsection

@section('footer_script')

    <!-- start - This is for export functionality only -->
        <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
        <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
        <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
        <!-- end - This is for export functionality only -->




        <script>
            $(function() {
                $('#product-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '{{route('datatables.tag')}}',
                    order: [ [0, 'desc'] ],
                    columns: [
                        { data: 'name', name: 'name' },
                        { data: 'created_at', name: 'created_at' },
                        {data: 'action', name: 'action', orderable: false, searchable: false},

                    ]
                });

            });
        </script>


@endsection