@extends('admin.base')

@section('content')
    <link href="{{asset('js/all-fine-uploader/fine-uploader-gallery.css')}}" rel="stylesheet">
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Slider</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">pages</li>
                    <li class="breadcrumb-item active">All</li>
                </ol>
            </div>
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-12">


                    <div class="card">
                        <div class="card-body">


                            <div>
                                <h3>Priority 1 </h3>
                                <div id="uploader1"></div>
                            </div>

                        </div>
                    </div>


                    <div class="card">
                        <div class="card-body">


                            <div>
                                <h3>Priority 2 </h3>
                                <div id="uploader2"></div>
                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">


                            <div>
                                <h3>Priority 3 </h3>
                                <div id="uploader3"></div>
                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">


                            <div>
                                <h3>Priority 4 </h3>
                                <div id="uploader4"></div>
                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">


                            <div>
                                <h3>Priority 5 </h3>
                                <div id="uploader5"></div>
                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">


                            <div>
                                <h3>Priority 6 </h3>
                                <div id="uploader6"></div>
                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">


                            <div>
                                <h3>Priority 7 </h3>
                                <div id="uploader7"></div>
                            </div>

                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">


                            <div>
                                <h3>Priority 8 </h3>
                                <div id="uploader8"></div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right sidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->

            <!-- ============================================================== -->
            <!-- End Right sidebar -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->

        @endsection


        @section('footer_script')




            <script src="{{asset('js/all-fine-uploader/all.fine-uploader.js')}}"></script>
            <script type="text/template" id="qq-template">
                <div class="qq-uploader-selector qq-uploader qq-gallery"
                     qq-drop-area-text=" To Replace Drop files here">
                    <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                             class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
                    </div>
                    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                        <span class="qq-upload-drop-area-text-selector"></span>
                    </div>
                    <div class="qq-upload-button-selector qq-upload-button">

                        <div>Upload</div>
                    </div>
                    <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
                    <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite"
                        aria-relevant="additions removals">
                        <li>
                            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                            <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                                     class="qq-progress-bar-selector qq-progress-bar"></div>
                            </div>
                            <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                            <div class="qq-thumbnail-wrapper">
                                <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                            </div>
                            <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                            <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                                <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                                Retry
                            </button>

                            <div class="qq-file-info">
                                <div class="qq-file-name">
                                    <span class="qq-upload-file-selector qq-upload-file"></span>
                                    <span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon"
                                          aria-label="Edit filename"></span>
                                </div>
                                <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                                <span class="qq-upload-size-selector qq-upload-size"></span>
                                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                                    <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                                </button>
                                <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                                    <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                                </button>
                                <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                                    <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                                </button>
                            </div>
                        </li>
                    </ul>

                    <dialog class="qq-alert-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">Close</button>
                        </div>
                    </dialog>

                    <dialog class="qq-confirm-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">No</button>
                            <button type="button" class="qq-ok-button-selector">Yes</button>
                        </div>
                    </dialog>

                    <dialog class="qq-prompt-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <input type="text">
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">Cancel</button>
                            <button type="button" class="qq-ok-button-selector">Ok</button>
                        </div>
                    </dialog>
                </div>
            </script>

            <script>
                // Some options to pass to the uploader are discussed on the next page
                /*var uploader = new qq.FineUploader({
                    element: document.getElementById("uploader")
                })*/

                var uploader = new qq.FineUploader({
                    request: {
                        endpoint: '{{route('ads.post.image.save',1)}}'
                    },
                    element: document.getElementById("uploader1")
                });
            </script>


            <script>
                // Some options to pass to the uploader are discussed on the next page
                /*var uploader = new qq.FineUploader({
                    element: document.getElementById("uploader")
                })*/

                var uploader = new qq.FineUploader({
                    request: {
                        endpoint: '{{route('ads.post.image.save',2)}}'
                    },
                    element: document.getElementById("uploader2")
                });
            </script>





            <script>
                // Some options to pass to the uploader are discussed on the next page
                /*var uploader = new qq.FineUploader({
                    element: document.getElementById("uploader")
                })*/

                var uploader = new qq.FineUploader({
                    request: {
                        endpoint: '{{route('ads.post.image.save',3)}}'
                    },
                    element: document.getElementById("uploader3")
                });
            </script>

            <script>
                // Some options to pass to the uploader are discussed on the next page
                /*var uploader = new qq.FineUploader({
                    element: document.getElementById("uploader")
                })*/

                var uploader = new qq.FineUploader({
                    request: {
                        endpoint: '{{route('ads.post.image.save',4)}}'
                    },
                    element: document.getElementById("uploader4")
                });
            </script>

            <script>
                // Some options to pass to the uploader are discussed on the next page
                /*var uploader = new qq.FineUploader({
                    element: document.getElementById("uploader")
                })*/

                var uploader = new qq.FineUploader({
                    request: {
                        endpoint: '{{route('ads.post.image.save',5)}}'
                    },
                    element: document.getElementById("uploader5")
                });
            </script>

            <script>
                // Some options to pass to the uploader are discussed on the next page
                /*var uploader = new qq.FineUploader({
                    element: document.getElementById("uploader")
                })*/

                var uploader = new qq.FineUploader({
                    request: {
                        endpoint: '{{route('ads.post.image.save',6)}}'
                    },
                    element: document.getElementById("uploader6")
                });
            </script>

            <script>
                // Some options to pass to the uploader are discussed on the next page
                /*var uploader = new qq.FineUploader({
                    element: document.getElementById("uploader")
                })*/

                var uploader = new qq.FineUploader({
                    request: {
                        endpoint: '{{route('ads.post.image.save',7)}}'
                    },
                    element: document.getElementById("uploader7")
                });
            </script>


            <script>
                // Some options to pass to the uploader are discussed on the next page
                /*var uploader = new qq.FineUploader({
                    element: document.getElementById("uploader")
                })*/

                var uploader = new qq.FineUploader({
                    request: {
                        endpoint: '{{route('ads.post.image.save',8)}}'
                    },
                    element: document.getElementById("uploader8")
                });
            </script>


















@endsection