@extends('admin.base')

@section('content')

    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Animation</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">pages</li>
                    <li class="breadcrumb-item active">Animation</li>
                </ol>
            </div>
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create New Category</h4>
                            <h6 class="card-subtitle">Donot Re create Same Category</h6>
                            <form class="form p-t-20"

                                  role="form" method="post"
                                  action="{{ route('category.update',$cat->id) }}"
                            >
                                <input name="_method" type="hidden" value="PUT">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputuname">Category Name</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-marker-alt"></i></div>
                                        <input type="text" class="form-control" value="{{$cat->name}}" name="name" placeholder="Category Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Category Slug</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-vector"></i></div>
                                        <input type="text" class="form-control" value="{{$cat->slug}}" name="slug" placeholder="Category Slug">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="pwd1">Category Description</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-more-alt"></i></div>
                                        <input type="text" class="form-control" value="{{$cat->description}}" name="description" placeholder="Category Description ">
                                    </div>
                                </div>


                                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                                <button type="reset" class="btn btn-inverse waves-effect waves-light">Cancel</button>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right sidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->

            <!-- ============================================================== -->
            <!-- End Right sidebar -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->

    @endsection