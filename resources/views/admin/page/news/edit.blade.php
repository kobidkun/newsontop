@extends('admin.base')

@section('content')
    <link href="{{asset('js/all-fine-uploader/fine-uploader-gallery.css')}}" rel="stylesheet">
    <style>
        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
        }
        .text-center{
            text-align: center;
        }
    </style>

    <style>
        .ck-editor__editable {
            min-height: 800px;
        }
    </style>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">News</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">News</li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </div>
            <div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->

            <form class="form p-t-20"

                  role="form" method="post"
                  action="{{ route('news.update',$n->id) }}"
            >
                <input name="_method" type="hidden" value="PUT">

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">


                                <div class="row">
                                    <div class="col-4">
                                <h4 class="card-title">Create New News</h4>


                                </div>


                                    <div class="col-2">

                                        <a href="{{route('post.twitter',$n->id)}}" class="btn btn-success">
                                            <i class="fa fa-twitter"></i> Post To Twitter</a>


                                    </div>

                                    <div class="col-2">

                                        <a href="{{route('post.facebookpage',$n->id)}}" class="btn btn-info">
                                            <i class="fa fa-facebook"></i> Post To Facebook</a>


                                    </div>


                                    <div class="col-2">

                                    <a  href="{{route('news.show',$n->id)}}"  target="_blank" class="btn btn-primary"><i class="fa fa-arrow-right"></i> Preview</a>


                                </div>


                                    <div class="col-2">


                                        @if ($n->activate === '0')
                                            <a href="{{route('activate.news',$n->id)}}"  class="btn btn-rounded btn-block btn-success">Activate</a>

                                        @elseif ($n->activate === '1')
                                            <a href="{{route('deactivate.news',$n->id)}}"  class="btn btn-rounded btn-block btn-danger">De Activate</a>

                                        @else

                                        @endif
                                </div>

                                </div>





                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputuname">News Title</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-marker-alt"></i></div>
                                        <input type="text"  class="form-control" name="title"
                                               value="{{$n->title}}"
                                               placeholder="News Title">
                                    </div>
                                </div>


                                <div class="form-group">

                                    <textarea class="ck-editor__editable" name="description" id="my-editor">



                     {!! $n->description !!}


                                    </textarea>
                                </div>


                                <div class="form-group">

                                    <button type="submit" class="btn waves-effect waves-light btn-block btn-info">Save Post</button>

                                </div>


                            </div>
                        </div>

                    </div>
                </div>








                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="card">
                            <div class="card-body">



                                <h4 class="card-title text-center">Primary Image</h4>

                                <img  class="center" src="{{asset('storage/'.$n->post_to_primary_imagebgs->location)}}" width="300px" alt="">


                                <br>
                                <br>



                                <div id="uploader"></div>



                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title text-center">Secondary Image</h4>


                                <img class="center" src="{{asset($n->post_to_primary_image_smalls->location)}}" width="200px" alt="">

                                <br>
                                <br>


                                <div id="uploadersecondary">



                                </div>










                            </div>
                        </div>
                    </div>
                </div>


















            </form>

        </div>


    @endsection

    @section('footer_script')

        <!-- start - This is for export functionality only -->

            <script src="{{asset('js/all-fine-uploader/all.fine-uploader.js')}}"></script>
            <script type="text/template" id="qq-template">
                <div class="qq-uploader-selector qq-uploader qq-gallery" qq-drop-area-text=" To Replace Drop files here">
                    <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"
                             class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
                    </div>
                    <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                        <span class="qq-upload-drop-area-text-selector"></span>
                    </div>
                    <div class="qq-upload-button-selector qq-upload-button">

                        <div>Upload</div>
                    </div>
                    <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
                    <ul class="qq-upload-list-selector qq-upload-list" role="region" aria-live="polite" aria-relevant="additions removals">
                        <li>
                            <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                            <div class="qq-progress-bar-container-selector qq-progress-bar-container">
                                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                            </div>
                            <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                            <div class="qq-thumbnail-wrapper">
                                <img class="qq-thumbnail-selector" qq-max-size="120" qq-server-scale>
                            </div>
                            <button type="button" class="qq-upload-cancel-selector qq-upload-cancel">X</button>
                            <button type="button" class="qq-upload-retry-selector qq-upload-retry">
                                <span class="qq-btn qq-retry-icon" aria-label="Retry"></span>
                                Retry
                            </button>

                            <div class="qq-file-info">
                                <div class="qq-file-name">
                                    <span class="qq-upload-file-selector qq-upload-file"></span>
                                    <span class="qq-edit-filename-icon-selector qq-btn qq-edit-filename-icon" aria-label="Edit filename"></span>
                                </div>
                                <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                                <span class="qq-upload-size-selector qq-upload-size"></span>
                                <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">
                                    <span class="qq-btn qq-delete-icon" aria-label="Delete"></span>
                                </button>
                                <button type="button" class="qq-btn qq-upload-pause-selector qq-upload-pause">
                                    <span class="qq-btn qq-pause-icon" aria-label="Pause"></span>
                                </button>
                                <button type="button" class="qq-btn qq-upload-continue-selector qq-upload-continue">
                                    <span class="qq-btn qq-continue-icon" aria-label="Continue"></span>
                                </button>
                            </div>
                        </li>
                    </ul>

                    <dialog class="qq-alert-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">Close</button>
                        </div>
                    </dialog>

                    <dialog class="qq-confirm-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">No</button>
                            <button type="button" class="qq-ok-button-selector">Yes</button>
                        </div>
                    </dialog>

                    <dialog class="qq-prompt-dialog-selector">
                        <div class="qq-dialog-message-selector"></div>
                        <input type="text">
                        <div class="qq-dialog-buttons">
                            <button type="button" class="qq-cancel-button-selector">Cancel</button>
                            <button type="button" class="qq-ok-button-selector">Ok</button>
                        </div>
                    </dialog>
                </div>
            </script>

            <script>
                // Some options to pass to the uploader are discussed on the next page
                /*var uploader = new qq.FineUploader({
                    element: document.getElementById("uploader")
                })*/

                var uploader = new qq.FineUploader({
                    request: {
                        endpoint: '{{route('image.primary.upload',$n->id)}}'
                    },
                    element: document.getElementById("uploader")
                });
            </script>

            <script>
                // Some options to pass to the uploader are discussed on the next page
                /*var uploader = new qq.FineUploader({
                    element: document.getElementById("uploader")
                })*/

                var uploader = new qq.FineUploader({
                    request: {
                        endpoint: '{{route('image.secondary.upload',$n->id)}}'
                    },
                    element: document.getElementById("uploadersecondary")
                });
            </script>


            <script src="{{asset('js/switchery.min.js')}}"></script>
            <script src="{{asset('js/select2.full.min.js')}}"></script>

            <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
            <script src="{{asset('js/bootstrap-tagsinput.min.js')}}"></script>
            <script src="{{asset('js/jquery.bootstrap-touchspin.min.js')}}"></script>
            <script src="{{asset('js/jquery.multi-select.js')}}"></script>

            <script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>

            <script>
                var options = {
                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
                };
            </script>




            <script>
                CKEDITOR.replace('my-editor', options);
            </script>



            <script>
                $(document).ready(function() {
                    $('.js-example-basic-multiple').select2();
                });
            </script>






@endsection