@extends('admin.base')

@section('content')






    <link href="{{asset('assets/plugins/dropzone-master/dist/dropzone.css')}}" rel="stylesheet" type="text/css" />
    <!-- Custom CSS -->



    <style>
        .ck-editor__editable {
            min-height: 400px;
        }
    </style>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Create News</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">News</li>
                    <li class="breadcrumb-item active">Create News</li>
                </ol>
            </div>
            <div>







            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->

            <form class="form p-t-20"

                  role="form" method="post"
                  action="{{ route('news.store') }}"
                  enctype="multipart/form-data"
            >


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Create New News</h4>

                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="exampleInputuname">News Title</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-marker-alt"></i></div>
                                        <input type="text"  class="form-control title-news" name="title" placeholder="News Title">
                                    </div>
                                </div>


                                <div class="form-group">

                            <textarea class="ck-editor__editable" name="description" id="my-editor"></textarea>
                                </div>


                        </div>
                    </div>

                </div>
            </div>






                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="card">
                            <div class="card-body">



                                <div class="form-group">
                                    <label>Category</label>
                                    <select name="primary_category_to_post_id" class="form-control custom-select">
                                        <option>--Category--</option>


                                        @foreach($cs as $c)


                                        <option value="{{$c->id}}">{{$c->name}}</option>

                                            @endforeach
                                    </select>
                                </div>



                                {{--<div class="form-group">
                                    <label>Saved Tags</label>
                                <select class="select2 m-b-10 select2-multiple" style="width: 100%"
                                        multiple="multiple" data-placeholder="Choose">
                                    @foreach($ts as $t)
                                        <option value="AK">{{$t->name}}</option>

                                    @endforeach

                                </select>

                                </div>--}}
                                <div class="form-group">
                                    <label for="exampleInputuname">Slug</label>
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="ti-marker-alt"></i></div>
                                        <input type="text"  class="form-control category-slug"
                                               readonly name="slug" placeholder="Slug">
                                    </div>
                                </div>






                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Add news Tags</h4>



                                <div class="tags-default">
                                    <input type="text" value="" name="tags" data-role="tagsinput"
                                           placeholder="add tags" />



                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12">


                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Add BG Image</h4>



                                <div class="tags-default">
                                    <input type="file"
                                           class="form-control"
                                           required
                                           name="bgimage"
                                           value=""


                                    >



                                </div>


                            </div>
                        </div>


                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Add Small Image</h4>



                                <div class="tags-default">
                                    <input type="file"
                                           class="form-control"
                                           required
                                           name="smallimage"
                                           value=""


                                    >



                                </div>


                            </div>
                        </div>
                    </div>



                    <div class="col-lg-6 col-md-12">

                    </div>













                </div>









<button type="submit">Submit</button>








            </form>
            <!-- ============================================================== -->
            <!-- End PAge Content -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Right sidebar -->
            <!-- ============================================================== -->
            <!-- .right-sidebar -->

            <!-- ============================================================== -->
            <!-- End Right sidebar -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->

    @endsection

    @section('footer_script')

        <!-- start - This is for export functionality only -->


            <script src="{{asset('js/switchery.min.js')}}"></script>
            <script src="{{asset('js/select2.full.min.js')}}"></script>

            <script src="{{asset('js/bootstrap-select.min.js')}}"></script>
            <script src="{{asset('js/bootstrap-tagsinput.min.js')}}"></script>
            <script src="{{asset('js/jquery.bootstrap-touchspin.min.js')}}"></script>
            <script src="{{asset('js/jquery.multi-select.js')}}"></script>



            <script src="//cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>
            <script>
                var options = {
                    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
                    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
                };
            </script>




            <script>
                CKEDITOR.replace('my-editor', options);
            </script>



            <script>
                $(document).ready(function() {
                    $('.js-example-basic-multiple').select2();
                });
            </script>

            <script>
                $(document).ready(function(){
                    $(".title-news").keyup(function(){

                        var cat_name_val = $( this ).val();
                        var actualSlug = cat_name_val.replace(/ /g,'-').toLowerCase()

                        $(".category-slug").val(actualSlug);



                    });
                });
            </script>





@endsection