@extends('admin.base')

@section('content')
    <link href="{{asset('js/all-fine-uploader/fine-uploader-gallery.css')}}" rel="stylesheet">
    <style>
        .center {
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 50%;
        }
        .text-center{
            text-align: center;
        }
    </style>
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">News</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">News</li>
                    <li class="breadcrumb-item active">Preview</li>
                </ol>
            </div>
            <div>

            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->


            <div class="row">

                <div class="col-12">
                    <div class="card">
                        <div class="card-body">





                            <div class="row">


                            <div class="col-6">

                                <h4 class="card-title">  {{$n->title}}</h4>


                            </div>


                            <div class="col-2">


                            </div>

                            <div class="col-2">

                                <a href="{{route('news.edit',$n->id)}}" class="btn btn-primary"><i class="fa fa-arrow-right"></i> Edit
                                </a>


                            </div>

                            <div class="col-2">


                                @if ($n->activate === '0')
                                    <a href="{{route('activate.news',$n->id)}}"
                                       class="btn btn-rounded btn-block btn-success">Activate</a>

                                @elseif ($n->activate === '1')
                                    <a href="{{route('deactivate.news',$n->id)}}"
                                       class="btn btn-rounded btn-block btn-danger">De Activate</a>

                                @else

                                @endif
                            </div>


                            </div>




                        </div>

                    </div>


                </div>


            </div>








            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">


                               <h2> {{$n->title}}</h2>






                    <div>
                        {!! $n->description !!}
                    </div>



























        </div>
                    </div>
                </div>

            </div>






            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <div class="card">
                        <div class="card-body">



                            <h4 class="card-title text-center">Primary Image</h4>
                            <img  class="center" src="{{asset('storage/'.$n->post_to_primary_imagebgs->location)}}" width="300px" alt="">







                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title text-center">Secondary Image</h4>


                            <img class="center" src="{{asset($n->post_to_primary_image_smalls->location)}}" width="200px" alt="">










                        </div>
                    </div>
                </div>
            </div>


    @endsection

    @section('footer_script')

        <!-- start - This is for export functionality only -->




@endsection