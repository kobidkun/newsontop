@extends('admin.base')

@section('content')

    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h3 class="text-themecolor">Animation</h3>
            </div>
            <div class="col-md-7 align-self-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                    <li class="breadcrumb-item">pages</li>
                    <li class="breadcrumb-item active">Animation</li>
                </ol>
            </div>
            <div>
                <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10">
                    <i class="ti-settings text-white"></i></button>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Container fluid  -->
        <!-- ============================================================== -->
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <center>       {{$feedback->title}}</center>
                        <br>


                        <img src="/storage/{{$feedback->location}}" alt="" width="350px;">

                    </div>


                </div>


            </div>


            <div class="row">
                <div class="col-lg-12">
                    <div class="card">


                        <div class="card">

                            <table class="table">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">email</th>
                                    <th scope="col">mobile</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Image</th>
                                </tr>
                                </thead>
                                <tbody>


                                @foreach($details as $feed)



                                    <tr>
                                        <th scope="row">
                                            {{$feed->id}}</th>
                                        <td>{{$feed->name}}</td>
                                        <td>{{$feed->email}}</td>
                                        <td> {{$feed->mobile}}</td>
                                        <td> {{$feed->description}}</td>


                                        <td>


                                            <img src="data:image/png;base64, {{   $feed->location }}"
                                                 alt="Image Preview"
                                                 width="250px;" height="auto"/>


                                        </td>
                                    </tr>



                                @endforeach


                                </tbody>
                            </table>





                        </div>


                    </div>


                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->

                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->

@endsection