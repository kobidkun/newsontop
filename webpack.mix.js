let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');*/

mix.styles([
    'resources/assets/admin/css/bootstrap.min.css',
   // 'resources/assets/admin/css/style.css',
    'resources/assets/admin/css/red.css'
], 'public/css/admin.min.css');


mix.scripts([
    'resources/assets/admin/js/jquery.min.js',
    'resources/assets/admin/js/popper.min.js',
    'resources/assets/admin/js/bootstrap.min.js',
    'resources/assets/admin/js/jquery.slimscroll.js',
    'resources/assets/admin/js/waves.js',
    'resources/assets/admin/js/sidebarmenu.js',
    'resources/assets/admin/js/sticky-kit.min.js',
    'resources/assets/admin/js/jquery.sparkline.min.js',
    'resources/assets/admin/js/custom.min.js'
], 'public/js/admin.min.js');


