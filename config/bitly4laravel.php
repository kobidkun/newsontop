<?php
/**
 * Configuraton file for bitly4laravel  BITLY_ACCESS_TOKEN
 * Populate only the needed fields and comment/remove the others
 */
return array(
    "access_token" => env('BITLY_ACCESS_TOKEN', 'f273aadb501a5817c57d5be1d314fa8306886dfd'),
    "cache_enabled" => false,
    "cache_duration" => 3600, // Duration in minutes
    "cache_key_prefix" => "Bitly4Laravel.",
    "response_format" => "json", // json, xml
    "request_type" => "get", // get, post
    "request_options" => array(),
    "client_config" => array(),
);
