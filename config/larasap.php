<?php

return [

    'telegram' => [
        'api_token' => '',
        'bot_username' => '',
        'channel_username' => '', // Channel username to send message
        'channel_signature' => '' // This will be assigned in the footer of message
    ],

    'twitter' => [
        'consurmer_key' => env('T_C_KEY', ''),
        'consurmer_secret' => env('T_C_SEC', ''),
        'access_token' => env('T_A_TOK', ''),
        'access_token_secret' => env('T_T_SEC', '')
    ],

    'facebook' => [
        'app_id' => '1534828060155519',
        'app_secret' => 'f1c85b28152454aca56d51b9199473b1',
        'default_graph_version' => '',
        'page_access_token' => env('FB_PAGE_TOKEN', ''),

    ]

];