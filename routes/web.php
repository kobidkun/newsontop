<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});


Route::get('/test', function () {
    return view('admin.page.blank');
});


Route::resource('dashboard/news', 'News\ManageNews');
Route::resource('dashboard/category', 'Category\ManageCategory');
Route::resource('dashboard/tag', 'Tag\ManageTag');


Route::get('datatables/category', 'Category\ManageCategory@datatables')->name('datatables.cat');
Route::get('datatables/tag', 'Tag\ManageTag@datatables')->name('datatables.tag');
Route::get('datatables/news', 'News\ManageNews@datatables')->name('datatables.news');
Route::get('delete/delete/{id}', 'News\ManageNews@destroy')->name('delete.news');
Route::get('post/activate/{id}', 'News\ManageNews@activate')->name('activate.news');
Route::get('post/deactivate/{id}', 'News\ManageNews@deactivate')->name('deactivate.news');
Route::get('post/facebookpage/{id}', 'News\ManageNews@PostFacebook')->name('post.facebookpage');
Route::get('post/twitter/{id}', 'News\ManageNews@Posttwitter')->name('post.twitter');


///slider
///
///
///
Route::get('slider/create', 'frontend\ManageSlider@Create')->name('slider.create');
Route::post('slider/create', 'frontend\ManageSlider@Store')->name('slider.store');
Route::get('slider', 'frontend\ManageSlider@Index')->name('slider.index');
Route::get('slider/delete/{id}', 'frontend\ManageSlider@delete')->name('slider.delete');
Route::get('slider/datatable', 'frontend\ManageSlider@datatables')->name('slider.datatable');


//feedback


Route::get('feedback/create', 'Feedback\ManageFeedback@Create')->name('feedback.create');
Route::post('feedback/create', 'Feedback\ManageFeedback@Store')->name('feedback.store');
Route::get('feedback', 'Feedback\ManageFeedback@Index')->name('feedback.index');
Route::get('feedback/delete/{id}', 'Feedback\ManageFeedback@delete')->name('feedback.delete');
Route::get('feedback/details/{id}', 'Feedback\ManageFeedback@details')->name('feedback.details');
Route::get('feedback/datatable', 'Feedback\ManageFeedback@datatables')->name('feedback.datatable');


////ads
///
///

Route::get('ads/manageads', 'Ad\Manageads@viewPage')->name('ads.manage');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
