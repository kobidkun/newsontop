<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('image/primary/upload/{id}', 'PublicController\ManageNews@PrimaryImageUpload')->name('image.primary.upload');
Route::post('image/secondary/upload/{id}', 'PublicController\ManageNews@SecondaryImageUpload')->name('image.secondary.upload');

Route::get('/allnews', 'PublicController\ManageNews@GetAllNews');
Route::get('/newsby/category/{slug}', 'PublicController\ManageNews@Categorywise');
Route::get('/newsby/category/id/{id}', 'PublicController\ManageNews@Categorywisebyid');
Route::get('/news/details/{slug}', 'PublicController\ManageNews@Details');

//slider


//ads

Route::post('ads/image/post/{id}', 'Ad\Manageads@Uploadsadsimage')->name('ads.post.image.save');
Route::get('ads/get/{id}', 'Ad\Manageads@adsbyid')->name('ads.getby.ad');




Route::get('/slider/homepage', 'PublicController\Slider@HomepageSlider');

//feedback

Route::get('/feedback/get', 'PublicController\Feedback@ViewFeedback');
Route::post('/feedback/user/post', 'PublicController\Feedback@UserResponse');
